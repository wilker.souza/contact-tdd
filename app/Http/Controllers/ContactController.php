<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveContactRequest;
use App\Http\Resources\ContactResource;
use App\Models\File;
use App\Models\Contact;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Create a new Contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SaveContactRequest $request)
    {
        $validated = $request->validated();

        $contact = (object)$request->all();

        $file = $request->file('anexo');
        $name = $file->getClientOriginalName();
        $upload = $file->storeAs('attachments', $file->getClientOriginalName());

        if ( ! $upload )
        {
            return redirect()->back()->with('error', 'Falha ao fazer upload')->withInput();
        }

        $dados = [
            'nome' => $contact->nome,
            'email' => $contact->email,
            'telefone' => $contact->telefone,
            'anexo' => $name,
            'mensagem' => $contact->mensagem,
            'ip' =>  $request->ip()
        ];

        Contact::create($dados);

        \Mail::to(env('MAIL_TO_ADDRESS'))
        ->send(new \App\Mail\contactMail($dados));

        return back()->with('success','Contato Enviado!');
    }
}
